void main() {
  var winningAppsSince2012 = [
    "FNB",
    "SnapScan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula ecosystem",
    "Naked Insurance",
    "EasyEquities",
    "Edtech Ambani Africa"
  ];

  winningAppsSince2012.sort();
  print("Winning apps since 2012: $winningAppsSince2012");

  print(
      "Winning App of the 2017 AppOfTheYear Awards is ${winningAppsSince2012[5]} and the winning App of the 2018 AppOfTheYear Awards is ${winningAppsSince2012[6]}");

  print(
      "Total number of Apps from the Array is ${winningAppsSince2012.length}.");
}
