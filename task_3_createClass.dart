void main() {
  var edtech = new bestApps();
  edtech.nameOfApp = "EdTech Ambani Africa";
  edtech.sectorCatergory =
      "Best Gaming Solution, Best Educational, and Best South African solution";
  edtech.developer = "Mukundi Lambani";
  edtech.yearWon = 2021;

  edtech.printBestAppsInformation();
}

class bestApps {
  String? nameOfApp;
  String? sectorCatergory;
  String? developer;
  int? yearWon;

  void printBestAppsInformation() {
    print("");
    print("Name of App: ${nameOfApp?.toUpperCase()}");
    print("Sector/Catergory: $sectorCatergory");
    print("Developer: $developer");
    print("Year won: $yearWon");
  }
}
